extends SGRayCast2D


func _ready():
	update_raycast_collision()
	if is_colliding():
		print(get_collider())  # Works as expected in v1.0.0-alpha9, but not in 1.0.0-alpha10.
