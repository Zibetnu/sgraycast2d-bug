This is a project demonstrating what I believe to be a bug with SGRayCast2D's collision detection in SG Physics 2D v1.0.0-alpha10 (https://gitlab.com/snopek-games/sg-physics-2d).

When running this project in v1.0.0-alpha9, the SGRayCast2D successfully returns the SGArea2D as the collider. However, running this project in v1.0.0-alpha10 only returns "Object:null" as the collider.
